# libenv

C API for reinforcement learning environments

Environment libraries are normal C shared libraries, providing
the interface described here.  Each library must implement all
functions.

See [`libenv.h`](libenv/libenv.h) for the interface.
